import axios from "axios";
export const login = async (credentials) => {
  return await axios
    .post("/auth/admin/login", {
      email: credentials.email,
      password: credentials.password,
    })
    .then((result) => ({
      result: result.data.response,
      error: undefined,
    }))
    .catch((err) => {
      return { result: undefined, error: err };
    });
};
