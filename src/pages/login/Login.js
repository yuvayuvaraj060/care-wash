import React from "react";
import { connect } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";
import { loginAuth } from "../../actions/index.js";
// css import

import "../../plugins/fontawesome-free/css/all.min.css";
import "../../plugins/icheck-bootstrap/icheck-bootstrap.min.css";
import "../../dist/css/adminlte.min.css";
import { useState } from "react";

export const Login = ({ loginAuth }) => {
  const [error, setError] = useState(undefined);

  const {
    register,
    handleSubmit,
    // formState: { errors },
  } = useForm();

  const history = useHistory();

  const login = async (data) => {
    const result = await loginAuth(data);
    console.log("🚀 ~ file: Login.js ~ line 26 ~ login ~ result", result);

    if (result === "success") history.push("/");
    try {
      if (result[0].msg) {
        setError(result[0].msg);
        return;
      }
      throw new error("error");
    } catch {
      setError(result);
    }
  };

  return (
    <div className="hold-transition login-page">
      <div className="login-box">
        {/* <!-- /.login-logo --> */}
        <div className="card card-outline card-primary">
          <div className="card-header text-center">
            <h1 className="h1">Car Wash</h1>
          </div>
          <div className="error" style={{ margin: "20px" }}>
            {error && (
              <p style={{ color: "red", textAlign: "center", margin: "0" }}>
                {error}
              </p>
            )}
          </div>
          <div className="card-body">
            <p className="login-box-msg">Sign in to start your session</p>
            <form onSubmit={handleSubmit(login)}>
              <div className="input-group mb-3">
                <input
                  type="email"
                  className="form-control"
                  placeholder="Email"
                  {...register("email")}
                />
                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-envelope"></span>
                  </div>
                </div>
              </div>
              <div className="input-group mb-3">
                <input
                  type="password"
                  className="form-control"
                  placeholder="Password"
                  {...register("password")}
                />
                <div className="input-group-append">
                  <div className="input-group-text">
                    <span className="fas fa-lock"></span>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-8">
                  <div className="icheck-primary">
                    <input type="checkbox" id="remember" />
                    <label htmlFor="remember">Remember Me</label>
                  </div>
                </div>
                {/* <!-- /.col --> */}
                <div className="col-4">
                  <button type="submit" className="btn btn-primary btn-block">
                    Sign In
                  </button>
                </div>
                {/* <!-- /.col --> */}
              </div>
            </form>
            <div className="social-auth-links text-center mt-2 mb-3">
              <Link to="#" className="btn btn-block btn-primary">
                <i className="fab fa-facebook mr-2"></i> Sign in using Facebook
              </Link>
              <Link to="#" className="btn btn-block btn-danger">
                <i className="fab fa-google-plus mr-2"></i> Sign in using
                Google+
              </Link>
            </div>
            {/* <!-- /.social-auth-links --> */}
            <p className="mb-1">
              <a href="forgot-password.html">I forgot my password</a>
            </p>
            <p className="mb-0">
              <a href="register.html" className="text-center">
                Register a new membership
              </a>
            </p>
          </div>
          {/* <!-- /.card-body --> */}
        </div>
        {/* <!-- /.card --> */}
      </div>
    </div>
  );
};

export default connect(null, { loginAuth })(Login);
