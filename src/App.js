import { BrowserRouter, Route, Switch } from "react-router-dom";
import LogIn from "./pages/login/Login.js";
import HomePage from "./pages/Home.jsx";
function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          {/* Home page */}
          <Route exact path="/" render={() => <HomePage />} />

          {/* Login Page */}
          <Route path="/login" render={() => <LogIn />} />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
