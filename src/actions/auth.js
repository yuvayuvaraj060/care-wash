import { login } from "../services/index.js";
import { authTypes } from "../action-type/index.js";

export const loginAuth = (credentials) => {
  return async (dispatch) => {
    const result = await login(credentials);
    if (result.result) {
      localStorage.setItem("token", `Bearer ${result.result.token}`);
      dispatch({
        type: authTypes.LOGIN,
        data: result.result,
      });
      return "success";
    }
    console.log(
      "🚀 ~ file: auth.js ~ line 13 ~ return ~ result.error",
      result.error.response
    );
    return result.error.response.data.response;
  };
};
