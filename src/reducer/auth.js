import { authTypes } from "../action-type/index.js";

const init = {
  _id: "",
  access: "",
  token: "",
};

export const auth = (state = init, action) => {
  switch (action.type) {
    case authTypes.LOGIN:
      return action.data;

    default:
      return state;
  }
};
